#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int check_arguments(int);
int read_all();
int append_to_file(char *);
int delete(char *);
int help();


int main(int argc, char *argv[]) {

    if (check_arguments(argc) != 0) {
        help();
        return(7);
    }

    // variables
    int i;

    // loop through command line args
    for (i = 1; i < (argc - 1); i++) {

        if (strcmp("-r", argv[i]) == 0) {
            if (read_all() == 2)
                return 2;
            continue;
        }

        if (strcmp("-a", argv[i]) == 0) {
            if (append_to_file(argv[++i]) == 2)
                return 2;
            continue;
        }

        if (strcmp("-d", argv[i]) == 0) {
            if (delete(argv[++i]) == 2)
                return 2;
            continue;
        }

        return help();
    }

    return 0;
}


int check_arguments( int arg_count ) {
    if ( arg_count > 3 ) {
        printf("error: too many arguments.\n");
        return 7;
    }
    return 0;
}

int read_all() {
    FILE *fp;
    char buffer[128];

    fp = fopen("books.txt", "r");
    
    if (fp == NULL) {
        printf("error: file not found.\n");
        return 2;
    }

    while (fgets(buffer, 128, fp)) {
        printf("%s", buffer);
    }
    
    fclose(fp);
}

int append_to_file(char title[]) {
    FILE *fp;
    char n_title[128];

    strcpy(n_title, title);
    strcat(n_title, "\n");

    fp = fopen("books.txt", "a");
    if ( fp == NULL) {
        printf("error: file not found.\n");
        return 2;
    }

    fputs(n_title, fp);
    fclose(fp);
}

int delete(char title[128]) {
    FILE *fp;
    char buffer[128];
    char all_titles[128][128];
    int i, j, k;
    int found = 0;

    fp = fopen("books.txt", "r");
    if ( fp == NULL) {
        printf("error: file not found.\n");
        return 2;
    }

    i = -1;
    while (fgets(buffer, 128, fp)) {
        i++;
        buffer[strcspn(buffer, "\n")] = '\0';
        if (strcmp(buffer, title) == 0) {
            found = 1;
            continue;
        } else {
            strcpy(all_titles[i], buffer);
        }
    }
    fclose(fp);

    if (found) {
        fp = fopen("books.txt", "w");
        for (j = 0; j < i + 1; j++) {
            fputs(all_titles[j], fp);
        }
        fclose(fp);
        printf("Title has been deleted\n");

    } else {
        printf("Title not found\n");
        return 2;
    }
    return 0;
}


int help() {
    printf("Usage: books [option] [option value]\n");
    printf("Options:\n");
    printf("\t-r \tread all items\n");
    printf("\t-a [item]\tappend item\n");
    printf("\t-d [item]\tdelete item\n");
    return 1;
}
